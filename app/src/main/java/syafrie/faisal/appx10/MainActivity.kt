package syafrie.faisal.appx10

import android.content.Intent
import android.os.Bundle
import android.os.PersistableBundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*


class  MainActivity : AppCompatActivity(), View.OnClickListener {
    override fun onClick(v: View?) {
        when(v?.id){
            R.id.button ->{
                var intent = Intent(this, Prodi::class.java)
                startActivity(intent)
            }
            R.id.button2 ->{
                var intent2 = Intent(this, Mahasiswa::class.java)
                startActivity(intent2)
            }

        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        button.setOnClickListener(this)
        button2.setOnClickListener(this)
    }

}
