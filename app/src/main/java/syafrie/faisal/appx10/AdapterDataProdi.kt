package syafrie.faisal.appx10

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso

import kotlinx.android.synthetic.main.activity_prodi.*

class AdapterDataProdi(val dataProdi: List<HashMap<String,String>>, val mainActivity: Prodi) :
    RecyclerView.Adapter<AdapterDataProdi.HolderDataProdi>(){
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): AdapterDataProdi.HolderDataProdi {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.row_mhs,p0,false)
        return HolderDataProdi(v)
    }
    override fun getItemCount(): Int{
        return dataProdi.size
    }

    inner class HolderDataProdi(v: View) : RecyclerView.ViewHolder(v){
        val txIdProdi = v.findViewById<TextView>(R.id.txIdProdi)
        val txNamaProdi = v.findViewById<TextView>(R.id.txNamaProdi)

    }

    override fun onBindViewHolder(p0: AdapterDataProdi.HolderDataProdi, p1: Int) {
        val data : HashMap<String, String> = dataProdi.get(p1)
        p0.txIdProdi.setText(data.get("id_prodi"))
        p0.txNamaProdi.setText(data.get("nama_prodi"))






    }
}