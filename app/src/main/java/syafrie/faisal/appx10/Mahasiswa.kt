package syafrie.faisal.appx10

import android.app.Activity
import android.app.DatePickerDialog
import android.app.Dialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Response
import com.android.volley.Request
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_mhs.*
import org.json.JSONArray
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap


class Mahasiswa : AppCompatActivity(), View.OnClickListener {
    lateinit var mediaHelper : MediaHelper
    lateinit var mhsAdapter : AdapterDataMhs
    lateinit var prodiAdapter : ArrayAdapter<String>
    var daftarMhs = mutableListOf<HashMap<String,String>>()
    var daftarProdi = mutableListOf<String>()
    val url = "http://192.168.43.66/app-x0a-web/show_data.php"
    val url2 = "http://192.168.43.66/app-x0a-web/get_nama_prodi.php"
    val url3 = "http://192.168.43.66/app-x0a-web/query_upd_del_ins.php"
    var imStr = ""
    var pilihProdi =""

    var tahun = 0
    var bulan = 0
    var hari = 0
    var jam = 0
    var menit = 0
    var tanggal : String=""
    var varradio : String=""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_mhs)
        mhsAdapter = AdapterDataMhs(daftarMhs, this)
        mediaHelper = MediaHelper(this)
        val cal: Calendar = Calendar.getInstance()
        bulan=cal.get(Calendar.MONTH)+1
        hari=cal.get(Calendar.DAY_OF_MONTH)
        tahun=cal.get(Calendar.YEAR)
        tanggal="$hari-$bulan-$tahun"

        btnTgl.setOnClickListener(this)
        listMhs.layoutManager = LinearLayoutManager(this)
        prodiAdapter = ArrayAdapter(this, android.R.layout.simple_dropdown_item_1line,
            daftarProdi)
        spinProdi.adapter = prodiAdapter
        spinProdi.onItemSelectedListener = itemSelected

        imUpload.setOnClickListener(this)
        btnUpdate.setOnClickListener(this)
        btnDelete.setOnClickListener(this)
        btnFind.setOnClickListener(this)

        listMhs.adapter = mhsAdapter

        rg1.setOnCheckedChangeListener{ group, checkedId->
            when(checkedId){
                R.id.rblaki -> varradio ="L"
                R.id.rbperempuan -> varradio ="P"
            }

        }

    }
    val itemSelected = object : AdapterView.OnItemSelectedListener{
        override fun onNothingSelected(parent: AdapterView<*>?) {
            spinProdi.setSelection(0)
            pilihProdi=daftarProdi.get(0)
        }

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            pilihProdi=daftarProdi.get(position)
        }

    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.imUpload->{
                val intent = Intent()
                intent.setType("image/*")
                intent.setAction(Intent.ACTION_GET_CONTENT)
                startActivityForResult(intent,mediaHelper.getRcGallery())
            }
            R.id.btnTgl -> showDialog(20)
            R.id.btnInsert->{
                queryInsertUpdateDelete("insert")
            }
            R.id.btnUpdate->{
                queryInsertUpdateDelete("update")
            }
            R.id.btnDelete->{
                queryInsertUpdateDelete("delete")
            }
            R.id.btnFind->{
                showDataMhs(edNamaMhs.text.toString().trim())
            }
        }
    }

    fun queryInsertUpdateDelete(mode:String){
        val request = object : StringRequest(Method.POST,url3,
            Response.Listener{response->
                mhsAdapter.notifyDataSetChanged()
                val jsonObject = JSONObject(response)
                val error =jsonObject.getString("kode")
                if(error.equals("000")){
                    Toast.makeText(this, "Operasi berhasil",Toast.LENGTH_LONG).show()
                    showDataMhs("")
                }else{
                    Toast.makeText(this, "Operasi gagal",Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener{error->
                Toast.makeText(this, "Tidak dapat terhubung ke server",Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm= HashMap<String,String>()
                val nmFile = "DC"+ SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault())
                    .format(Date())+".jpg"
                when(mode){
                    "insert" -> {
                        hm.put("mode","insert")
                        hm.put("nim",edNim.text.toString())
                        hm.put("nama",edNamaMhs.text.toString())
                        hm.put("alamat",edAlamat.text.toString())
                        hm.put("jeniskel",varradio)
                        hm.put("tgllahir",tanggal)
                        hm.put("image",imStr)
                        hm.put("file",nmFile)
                        hm.put("nama_prodi",pilihProdi)

                    }
                    "update"->{
                        hm.put("mode","update")
                        hm.put("nim",edNim.text.toString())
                        hm.put("nama",edNamaMhs.text.toString())
                        hm.put("alamat",edAlamat.text.toString())
                        hm.put("jeniskel",varradio)
                        hm.put("tgllahir",tanggal)
                        hm.put("image",imStr)
                        hm.put("file",nmFile)
                        hm.put("nama_prodi",pilihProdi)
                    }
                    "delete"->{
                        hm.put("mode","delete")
                        hm.put("nim",edNim.text.toString())

                    }
                }
                return hm
            }
        }
        val queue =  Volley.newRequestQueue(this)
        queue.add(request)

    }

    override fun onStart() {
        super.onStart()
        getNamaProdi()
        showDataMhs("")
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == Activity.RESULT_OK){
            if(requestCode == mediaHelper.getRcGallery()){
                imStr = mediaHelper.getBitmapToString(data!!.data!!, imUpload)
            }
        }
    }

    var dateChangeDialog = DatePickerDialog.OnDateSetListener { view, year, month, dayOfMonth ->
        tanggal="$dayOfMonth-${month+1}-$year"
        tahun = year
        bulan = month+1
        hari = dayOfMonth
    }

    override fun onCreateDialog(id: Int): Dialog {
        when(id){

            20 -> return DatePickerDialog(this, dateChangeDialog, tahun,bulan,hari)

        }
        return super.onCreateDialog(id)
    }


    fun getNamaProdi(){
        val request = StringRequest(Request.Method.POST, url2,
            Response.Listener{ response ->
                daftarProdi.clear()
                val jsonArray = JSONArray(response)
                for (x in 0..(jsonArray.length()-1)) {
                    val jsonObject = jsonArray.getJSONObject(x)
                    daftarProdi.add(jsonObject.getString("nama_prodi"))
                }
                prodiAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener{error->}
        )
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun showDataMhs(namaMhs : String) {
        val request = object : StringRequest(
            Request.Method.POST, url,
            Response.Listener { response ->
                daftarMhs.clear()
                val jsonArray = JSONArray(response)
                for (x in 0..(jsonArray.length() - 1)) {
                    val jsonObject = jsonArray.getJSONObject(x)
                    var mhs = HashMap<String, String>()
                    mhs.put("nim", jsonObject.getString("nim"))
                    mhs.put("nama", jsonObject.getString("nama"))
                    mhs.put("nama_prodi", jsonObject.getString("nama_prodi"))
                    mhs.put("url", jsonObject.getString("url"))
                    mhs.put("jeniskel", jsonObject.getString("jeniskel"))
                    mhs.put("tgllahir", jsonObject.getString("tgllahir"))
                    mhs.put("alamat", jsonObject.getString("alamat"))
                    daftarMhs.add(mhs)
                }
                mhsAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this, "Terjadi kesalahan koneksi ke server", Toast.LENGTH_LONG).show()
            }){
                override fun getParams():MutableMap<String,String>{
                    val hm = HashMap<String,String>()
                    hm.put("nama",namaMhs)
                    return hm
                }
            }


        val queue = Volley.newRequestQueue(this)
        queue.add(request)

    }

    override fun finish(){
        var intent= Intent()
        super.finish()
    }
}